import React, { useContext } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import Button from '../core/Button';
import Label from '../core/Label';
import { AxiosContext } from '../../context/axios';

export default function LoginForm() {
	// Context
	// Axios - used for network requests
	const axiosContext = useContext(AxiosContext);

	function loginWithCredentials(credentials) {
		axiosContext.post('login/' + process.env.REACT_APP_API_VERSION, {
			"email": credentials.email,
			"password": credentials.password
		}).then((response) => {
			console.log(response);
		}).catch((error) => {
			console.log(error.response);
		});
	}

	const formik = useFormik({
		initialValues: {
			email: '',
			password: ''
		},
		validationSchema: Yup.object({
			email: Yup.string()
				.email('Invalid email address')
				.required('Required'),
			password: Yup.string()
				.min(5, 'Must be 5 characters or more')
				.required('Required')
		}),
		onSubmit: values => {
			loginWithCredentials({
				email: values.email,
				password: values.password
			});
			window.location = process.env.REACT_APP_BASENAME;
		},
	});

	const emailError = formik.touched.email && formik.errors.email;
	const passwordError = formik.touched.password && formik.errors.password;

	return (
		<form onSubmit={formik.handleSubmit} data-testid={'login-form'}>
			<Label for="email">Email Address</Label><br/>
			<input data-testid={'login-form-email'} name="email" id="email" {...formik.getFieldProps('email')} className={"form-control" + (emailError ? " input-error" : "")} /><br/>
			<Label for="password">Password</Label><br/>
			<input data-testid={'login-form-password'} name="password" id="password" {...formik.getFieldProps('password')} type="password" className={"form-control" + (passwordError ? " input-error" : "")} />
			<br/>
			<div className="flex">
				<div className="flex flex-50 flex-vertically-center">
					<Link to="/" style={{color: '#676767'}}>Forgot password?</Link>
				</div>
				<div className="flex flex-50 flex-vertically-center flex-align-right">
					<Button
						text="Log in"
						type="submit"
						color="blue"
						style={{'float': 'right'}}
						data-testid={'login-form-submit-button'}
					/>
				</div>
			</div>
		</form>
	);
}