import React from "react";
import { render } from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';
import Avatar from "./Avatar";

// Image passed in
it("Tests that when an image is sent that's what's used for the avatar, otherwise uses the default", () => {
  const { container } = render(
    <Avatar avatarImage={"assets/images/test.jpg"} />
  );
  expect(container).toMatchInlineSnapshot(`
    <div>
      <div
        data-testid="avatar-icon"
        id="user_avatar"
      >
        <img
          alt="Avatar"
          class="user-avatar image"
          src="assets/images/test.jpg"
        />
      </div>
    </div>
  `);
});

// No image passed in
it("Confirms default avatar has correct icon", async () => {
  const { getByTestId } = render(
    <Avatar avatarImage="" />
  );

  expect(getByTestId('avatar-icon').children[0].dataset.icon).toEqual('user-circle');
});
