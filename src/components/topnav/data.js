export const notificationsData = [
    {
        "id": 6,
        "data": {
            "text": "Form - (RFQ)",
            "date": "Thr Apr 19",
            "new": true
        }
    },
    {
        "id": 5,
        "data": {
            "text": "Transaction - ($10,000)",
            "date": "Thr Apr 19",
            "new": true
        }
    },
    {
        "id": 4,
        "data": {
            "text": "Transaction",
            "date": "Thr Apr 19",
            "new": true
        }
    },
    {
        "id": 3,
        "data": {
            "text": "Contact Form",
            "date": "Thr Apr 19",
            "new": false
        }
    },
    {
        "id": 2,
        "data": {
            "text": "Transaction",
            "date": "Thr Apr 19",
            "new": false
        }
    }
];

export const sites = [
    {
        value: 1,
        label: 'Ecreative'
    }, {
        value: 2,
        label: 'BettsHD'
    }, {
        value: 3,
        label: 'Skolnik'
    }, {
        value: 4,
        label: 'MAD Greenhouse'
    }, {
        value: 5,
        label: 'Magnet Shop'
    }
];
