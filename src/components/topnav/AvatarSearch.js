import React from 'react';
import SearchInput from '../core/SearchInput';
import styles from '../../css/Avatar.module.scss';

export default function AvatarSearch() {
    return (
        <>
            <div id={'avatar-search'}>
                <form id={'avatar-search-form'} className={styles.search_form}>
                    <SearchInput
                        inputID="avatarSearchInput"
                    />
                </form>
            </div>
        </>
    );
}