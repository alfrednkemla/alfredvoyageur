import React from 'react';
import SearchInput from '../core/SearchInput';

function TopNavSearch(props) {
	return (
		<>
			<div id={'top-nav-search'} onClick={() => props.handleSearchClick()}>
				<form id={'top-nav-search-form'}>
					<SearchInput
						inputID="topNavSearchInput"
					/>
				</form>
			</div>
		</>
	);
}

export default TopNavSearch;
