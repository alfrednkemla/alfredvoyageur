import React from 'react';
import Icon from '../core/Icon';
import styles from '../../css/NotificationList.module.scss';

export default function NotificationListItem(props) {
    return (
        <li
            id={"record_id_" + props.id}
            onClick={() => props.handleClick(props.id)}
        >
            <div className={styles.notificationNew}>
                {props.new && 
                    <div className={styles.newNotificationCircle}>
                        <Icon
                            icon={['fas', 'circle']}
                        />
                    </div>
                }
            </div>
            <div className={styles.notificationText}>
                <span className={'item-label'}>{props.text}</span>
            </div>
            <div className={styles.notificationDate}>
                <span className={'item-date'}>{props.date}</span>
            </div>
        </li>
    );
}
