import React from 'react';
import Icon from '../core/Icon';

export function Avatar(props) {
	return (
		<div id={'user_avatar'} data-testid="avatar-icon">
			{props.avatarImage !== '' ?
				<img
					alt={"Avatar"}
					className={'user-avatar image'}
					src={props.avatarImage}/> :
				<Icon
					class={'user-avatar default'}
					icon={['fas', 'user-circle']}/>}
		</div>
	);
}


export default Avatar;