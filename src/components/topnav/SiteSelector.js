import React, { useState } from 'react';
import { sites } from './data';
import Select from 'react-select';

export default function SiteSelector() {
	const options = sites;
	const [selectedOption, setSelectedOption] = useState(sites[0]);

	const customStyles = {
		container: (styles) => ({
			...styles,
			width: 'calc(100% - 50px)',
			display: 'inline-block',
			marginLeft: '15px',
			verticalAlign: 'middle',
			outline: 'none'
		}),
		option: (styles, { isFocused, isSelected }) => ({
			...styles,
			color: '#707070',
			background: isSelected ? '#DADADA' : isFocused ? '#ffffff' : null,
			':hover': {
				color: '#707070',
				background: '#DADADA',
				cursor: 'pointer'
			}
		}),
		control: (styles, { isFocused }) => ({
			...styles,
			color: '#e1e0e0',
			background: '#e1e0e0',
			':hover': {
				borderColor: 'gray'
			},
			borderColor: isFocused ? '#3B54A5' : null,
			boxShadow: 'none'
		}),
		input: (styles) => ({
			...styles,
			color: '#707070'
		}),
		menu: (styles) => ({
			...styles,
			background: '#e1e0e0'
		}),
		menuList: (styles) => ({
			...styles,
			background: '#ffffff',
			borderRadius: '5px'
		})
	}

	function handleChange(selectedOption) {
		setSelectedOption(selectedOption);
	}

	return (
		<Select
			styles={customStyles}
			value={selectedOption}
			options={options}
			onChange={handleChange}
		/>
	);
}
