import React, { useState, useEffect } from 'react';
import TopNavSearch from './TopNavSearch';
import Icons from "./Icons";
import Logo from "./Logo";
import SiteSelector from './SiteSelector';
import styles from '../../css/MenuPanel.module.scss';

function TopNav(props) {
	const [searchClicked, setSearchClicked] = useState(false);
	useEffect(() => {
		if (searchClicked === true) {
			setSearchClicked(false);
		}
	}, [searchClicked]);

	function sendClick() {
		setSearchClicked(!searchClicked);
	}

	return (
		<div id={'topNav'} data-testid="topNav">
			<div className={'no-gutters row'}>
				<div className={'col-2'} id={'topNavLeft'}>
					<div className={styles.logo_container}>
						<Logo />
						<SiteSelector />
					</div>
				</div>
				<div className={'col-10'} id={'topNavRight'}>
					<TopNavSearch handleSearchClick={sendClick} />
					<Icons
						bodyClicked={props.bodyClicked}
						closeOpenPanels={searchClicked}
						resizedDimensions={props.resizedDimensions}
					/>
				</div>
			</div>
		</div>
	);
}

export default TopNav;
