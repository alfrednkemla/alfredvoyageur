import React from 'react';
import FlyoutArrow from './FlyoutArrow';
import Icon from '../core/Icon';
import AvatarFlyout from './AvatarFlyout';
import NotificationList from './NotificationList';

export default function TopNavFlyout(props) {
	let flyoutContent;

	function TopNavFlyoutItems(props) {
		let flyoutItems;
		let listItems;
		switch (props.items) {
			case 'add-items':
				flyoutItems = [
					{
						"label": "Page",
						"icon": ['fas', 'file-alt']
					},
					{
						"label": "Lead",
						"icon": ['fas', 'hand-holding-usd']
					},
					{
						"label": "Menu",
						"icon": ['fab', 'elementor']
					},
					{
						"label": "Gallery",
						"icon": ['fas', 'images']
					},
					{
						"label": "Form",
						"icon": ['fas', 'columns']
					},
					{
						"label": "Asset",
						"icon": ['fas', 'cubes']
					}
				];
				listItems = flyoutItems.map((item, index) =>
					<li key={index}>
						<span className={'topnav-add-icon'}><Icon icon={item.icon} /></span>
						<span className={'topnav-add-label'}>{item.label}</span>
					</li>
				);
				flyoutContent = <ul>{listItems}</ul>

				break;
			case 'notification-items':
				flyoutContent = <NotificationList />
				break;
			case 'avatar-items':
				flyoutContent = <AvatarFlyout />;
				break;
			default:
				break;
		}
		return (
			<>
				{flyoutContent}
			</>
		);
	}

	return (
		<div data-testid={props.testid} className='flyout visible'>
			<FlyoutArrow location={"top"} />
			<TopNavFlyoutItems items={props.items} />
		</div>
	);
}
