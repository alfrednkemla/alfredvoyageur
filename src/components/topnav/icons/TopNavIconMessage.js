import React from 'react';
import TopNavIcon from './TopNavIcon';

export default function TopNavIconMessage(props) {
	return (
		<TopNavIcon topNavIcon={props.clicked ? ['far', 'envelope'] : ['fas', 'envelope']} class={props.class}/>
	);
}
