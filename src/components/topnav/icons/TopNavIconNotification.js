import React from 'react';
import TopNavIcon from './TopNavIcon';

export default function TopNavIconNotification(props) {
	return (
		<TopNavIcon topNavIcon={props.clicked ? ['far', 'bell'] : ['fas', 'bell']} class={props.class}/>
	);
}
