import React from 'react';
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';

import Logo from './Logo';

it('Verifies presence of logo', async ()=>{
    const { getByTestId } = render(<Router><Logo /></Router>);
    expect(getByTestId('admin_logo')).toHaveClass('App-logo');
});
