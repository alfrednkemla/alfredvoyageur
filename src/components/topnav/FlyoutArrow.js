import React from 'react';

function FlyoutArrow(props) {
	return(
		<>
			<div className={props.location !== undefined ? "flyout-arrow " + props.location : "flyout-arrow top"}>
				<svg
					width="23"
					height="22">
					<polygon
						points={"0,27 12,0 23,27"}
						fill={"#FFF"}
					/>
				</svg>
			</div>
		</>
	);
}

export default FlyoutArrow;