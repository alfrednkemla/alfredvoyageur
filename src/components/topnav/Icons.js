import React, { useState, useEffect } from 'react';
import TopNavIcon from './icons/TopNavIcon';
import TopNavIconPlus from './icons/TopNavIconPlus';
import TopNavIconMessage from './icons/TopNavIconMessage';
import TopNavIconNotification from './icons/TopNavIconNotification';
import Avatar from './Avatar';
import TopNavFlyout from './Flyout';

export default function Icons(props) {
	const [plusActive, setPlusActive] = useState(false);
	const [mailActive, setMailActive] = useState(false);
	const [notificationActive, setNotificationActive] = useState(false);
	const [showAvatarFlyout, setShowAvatarFlyout] = useState(false);

	useEffect(() => {
		if (props.bodyClicked || props.closeOpenPanels) {
			setPlusActive(false);
			setMailActive(false);
			setNotificationActive(false);
			setShowAvatarFlyout(false);
		}
	}, [props.bodyClicked, props.closeOpenPanels]);

	function toggleTopIcon(icon) {
		switch (icon) {
			case 'plus':
				setMailActive(false);
				setNotificationActive(false);
				setPlusActive(!plusActive);
				setShowAvatarFlyout(false);
				break;
			case 'envelope':
				setMailActive(!mailActive);
				setNotificationActive(false);
				setPlusActive(false);
				setShowAvatarFlyout(false);
				break;
			case 'bell':
				setMailActive(false);
				setNotificationActive(!notificationActive);
				setPlusActive(false);
				setShowAvatarFlyout(false);
				break;
			case 'avatar':
				setMailActive(false);
				setNotificationActive(false);
				setPlusActive(false);
				setShowAvatarFlyout(!showAvatarFlyout);
				break;
			default:
				setMailActive(false);
				setNotificationActive(false);
				setPlusActive(false);
				setShowAvatarFlyout(false);
				break;
		}
	}

	return (
		<>
			<div className={'top-nav-button-wrapper add'}>
				<button
					data-testid={'tn_plus'}
					onClick={() => toggleTopIcon('plus')}
					className={'icon-wrapper'}
				>
					<TopNavIconPlus clicked={plusActive} />
				</button>
				{plusActive &&
					<TopNavFlyout
						testid={'tnf_plus'}
						items={'add-items'}
					/>
				}
			</div>
			<div data-testid={'tn_mail'} className={'top-nav-button-wrapper'}>
				<button
					data-testid={'tn_message'}
					onClick={() => toggleTopIcon('envelope')}
					className={'icon-wrapper'}>
					<TopNavIconMessage
						clicked={mailActive}
					/>
				</button>
			</div>
			<div data-testid={'tn_notifications'} className={'top-nav-button-wrapper notifications'}>
				<button
					data-testid={'tn_bell'}
					onClick={() => toggleTopIcon('bell')}
					className={'icon-wrapper'}>
					<TopNavIconNotification
						clicked={notificationActive}
					/>
				</button>
				{notificationActive &&
					<TopNavFlyout
						testid={'tnf_bell'}
						items={'notification-items'}
					/>
				}
			</div>
			<div className={'top-nav-button-wrapper avatar'}>
				<button
					data-testid={'tn_avatar'}
					onClick={() => toggleTopIcon('avatar')}
					className={'icon-wrapper avatar-wrapper'}>
					<Avatar avatarImage="" />
				</button>
				{showAvatarFlyout &&
					<TopNavFlyout
						testid={'tnf_avatar'}
						items={'avatar-items'}
					/>
				}
			</div>
			<div data-testid={'tn_question'} className={'top-nav-button-wrapper'}>
				<button
					data-testid={'tn_help'}
					onClick={() => toggleTopIcon()}
					className={'icon-wrapper question'}>
					<TopNavIcon
						topNavIcon={['fas', 'question-circle']}
					/>
				</button>
			</div>
		</>
	);
}
