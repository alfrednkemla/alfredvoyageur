import React from 'react';
import logo from '../../assets/logo.svg';
import { Link } from 'react-router-dom';

function Logo() {
	return (
		<>
			<Link to='/'>
				<img data-testid={'admin_logo'} src={logo} className="App-logo img-responsive" alt="logo" style={{ width: '32px', height: 'auto'}} />
			</Link>
		</>
	);
}

export default Logo;
