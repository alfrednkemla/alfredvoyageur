import React, { useEffect, useState, useRef } from 'react';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import GoogleAnalytics from '../dashboard/GoogleAnalytics';
import useWindowSize from '../../../hooks/useWindowSize';

export default function ContentPanel(props) {
    const [width, setWidth] = useState(0);
    const contentPanelRef = useRef(null);
    const [windowWidth] = useWindowSize();

    useEffect(() => {
        // Need to set a timeout for the content panel CSS transition of 0.2s
        setTimeout(() => {
            setWidth(contentPanelRef.current.contentEl.clientWidth - 30);
        }, 200);
    }, [props.drawerIsOpen, props.menuPanelIsCollapsed, windowWidth]); // Need something here - another thing that will trigger the effect

    return (
        <>
            <h1>Content Panel</h1>
            <SimpleBar style={{height: 'calc(100% - 115px)'}} ref={contentPanelRef}>
                <button className="btn blue-btn" onClick={() => props.handleDrawer()}>{props.drawerIsOpen ? 'Close' : 'Open'} Drawer</button>
                <br></br><br></br>
                <GoogleAnalytics
                    containerWidth={width}
                />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In id luctus massa, vitae hendrerit ante. In iaculis justo in massa iaculis rhoncus. Ut ultrices tortor leo, eu rhoncus nunc scelerisque vitae. Praesent lobortis, orci at facilisis volutpat, ante nibh mollis odio, id rhoncus diam nunc ut lorem. Vivamus vitae placerat ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque sed tortor vestibulum, aliquet sapien quis, blandit diam. Aliquam augue ante, convallis non pharetra id, lacinia sed felis. Mauris sit amet eros viverra, viverra mauris ut, varius arcu. Suspendisse ut dignissim felis. Quisque vel volutpat nibh, a consequat mi.</p>
                <p>Nunc euismod eros et nibh accumsan tempus. In vitae quam nec tortor tempus varius eu eu dui. Sed posuere condimentum tempor. Fusce sed auctor neque. Nullam rhoncus sodales eros ut dapibus. Curabitur a nulla ac augue eleifend euismod et vel enim. Praesent vehicula finibus magna id facilisis. Cras sodales eros in metus aliquet lacinia. Nulla facilisi.</p>
                <p>Sed ut nisi semper, tempus massa nec, aliquet velit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla blandit consequat elit ut maximus. Nam vel orci scelerisque, ornare metus vel, malesuada purus. Mauris sed justo posuere velit molestie fringilla. Mauris odio odio, vestibulum eget volutpat sit amet, ornare nec augue. Duis egestas lacus nec mauris mattis mattis. Mauris malesuada rhoncus elit id sodales. Praesent quis aliquam elit, non dapibus nisl. Nulla tincidunt est diam, et convallis erat sollicitudin ac.</p>
                <p>Nunc quis diam ut nunc tincidunt malesuada. Morbi nulla lectus, suscipit sed mauris nec, finibus rutrum nibh. Duis bibendum eleifend eros et viverra. Duis ut aliquam leo. Vivamus eleifend mi arcu, ac iaculis arcu faucibus at. In vulputate metus vitae justo ultrices lobortis. Fusce vel sapien vel orci vestibulum pulvinar. Quisque nec dolor id ligula tincidunt pretium. Integer et sollicitudin nisi, eget malesuada diam. Phasellus non odio euismod sem suscipit convallis at semper quam. Praesent sed lorem dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ut blandit ligula. Vivamus porta dui ac tellus aliquam, ac interdum erat aliquet.</p>
                <p>Praesent tincidunt nibh lacus, nec egestas urna rhoncus id. Quisque maximus orci in mi finibus, et posuere urna egestas. Nam pretium nunc nunc, sed varius dolor ultrices quis. Ut quis sapien varius, varius ligula ornare, pharetra lorem. Proin tempor bibendum turpis a ullamcorper. Sed pellentesque, ex non tempus lacinia, est quam interdum metus, vitae faucibus augue orci quis elit. Etiam scelerisque nisi velit. Donec lacinia posuere sagittis. Aliquam bibendum turpis in tincidunt euismod. </p>
            </SimpleBar>
        </>
    );
}
