import React from 'react';

export default function Drawer(props) {
    return (
        <>
            {props.children}
        </>
    );
}
