import React, { useState } from 'react';
import MenuPanel from './menu-panel/index';
import ListPanel from './list-panel/index';
import ContentPanel from './content-panel/index';
import Drawer from './drawer/index'
import TabSet from '../core/TabSet';
import { tabs } from './data';

export default function Body(props) {
	const [drawerIsOpen, setDrawerIsOpen] = useState(false);
	const [menuPanelIsCollapsed, setMenuPanelIsCollapsed] = useState(false);

	function handleDrawer() {
		setDrawerIsOpen(!drawerIsOpen);
	}

	function handleMenu(isOpen) {
		setMenuPanelIsCollapsed(!isOpen);
	}

	return (
		<div data-testid="body" id="main-content" onClick={() => props.handleBodyClick()} >
			<div className="stretch-height flex" style={{alignItems: 'stretch'}}>
				<div className={'stretch-height'} id="menuPanel">
					<div className={'stretch-height'} id="menu-panel-container">
						<MenuPanel
							handleCollapse={handleMenu}
						/>
					</div>
				</div>
				<div className="stretch-height" id="listPanel">
					<div className={'stretch-height'} id="list-panel-container">
						<ListPanel />
					</div>
				</div>
				<div className="stretch-height" id={drawerIsOpen ? 'contentPanelDrawerOpen' : 'contentPanel'}>
					<div className={'stretch-height'} id="content-panel-container">
						<ContentPanel
							drawerIsOpen={drawerIsOpen}
							handleDrawer={handleDrawer}
							menuPanelIsCollapsed={menuPanelIsCollapsed}
						/>
					</div>
				</div>
				{drawerIsOpen && 
					<div className="stretch-height" id="drawer">
						<div className={'stretch-height'} id="drawer-panel-container">
							<Drawer>
								<TabSet 
									tabs={tabs}
								/>
							</Drawer>
						</div>
					</div>
				}
			</div>
		</div>
	);
}