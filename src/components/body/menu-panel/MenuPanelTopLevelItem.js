import React from 'react';
import styles from '../../../css/MenuPanel.module.scss';
import Icon from '../../core/Icon';
import useWindowSize from '../../../hooks/useWindowSize';

export default function MenuPanelTopLevelItem(props) {
    const [width] = useWindowSize();

    return (
        <li
            onClick={() => props.handleClick(props.type)}
            id={'mp_' + props.type}
            data-testid={'mp_' + props.type}
            className={props.activeElement.topLevel === props.type ? styles.list_item_selected : styles.list_item}
        >
            <Icon icon={props.icon} class={styles.mp_icon} />
            {width >= 992 && 
                <span>{props.text}</span>
            }
        </li>
    );
}
