import React from 'react';
import Icon from '../../core/Icon';
import styles from '../../../css/MenuPanel.module.scss';

export default function MenuPanelSubList(props) {
    const list = props.list.map((listItem) =>
        <li
            className={''}
            key={props.listID + '_' + listItem.sublist_id}
            id={props.listID + '_' + listItem.sublist_id}
            onClick={() => props.handleSublistClick(props.listID + '_' + listItem.sublist_id)}>
            <Icon icon={listItem.icon} />{listItem.sublist_label}
        </li>
    );
    return (
        <li>
            <ul
                id={props.listID}
                data-testid={props.listID}
                className={styles.sub_list}>
                {list}
            </ul>
        </li>
    );
}