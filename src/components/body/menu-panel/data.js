export const pagesList = [
    {
        "sublist_id": "manufacturers",
        "sublist_label": "Manufacturers",
        "icon": ['far', 'image'],

    },
    {
        "sublist_id": "blogs",
        "sublist_label": "Blogs",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "products",
        "sublist_label": "Products",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    }
];

export const leadsList = [
    {
        "sublist_id": "form_submissions",
        "sublist_label": "Form Submissions",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "click_to_calls",
        "sublist_label": "Click-to-Calls",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "mail_to_clicks",
        "sublist_label": "Mail-to-Clicks",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "clients",
        "sublist_label": "Clients",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    }
];

export const menuList = [
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    }
];

export const galleryList = [
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    }
];

export const formsList = [
    {
        "sublist_id": "contact_us",
        "sublist_label": "Contact Us",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "request_for_quote",
        "sublist_label": "Request for Quote",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "payment",
        "sublist_label": "Payment",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "reservation",
        "sublist_label": "Reservation",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "become_a_partner",
        "sublist_label": "Become a Partner",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    },
];

export const marketingList = [
    {
        "sublist_id": "paid_search",
        "sublist_label": "Paid Search",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "display_assets",
        "sublist_label": "Display Assets",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "video_assets",
        "sublist_label": "Video Assets",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "internal_promotion",
        "sublist_label": "Internal Promotion",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    }
];

export const assetsList = [
    {
        "sublist_id": "images",
        "sublist_label": "Images",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "documents",
        "sublist_label": "Documents",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "videos",
        "sublist_label": "Videos",
        "icon": ['far', 'image']
    },
    {
        "sublist_id": "settings",
        "sublist_label": "Settings",
        "icon": ['fas', 'cog']
    },
];

export const messagesList = [
    {
        "sublist_id" : "message_1",
        "sublist_label" : "Message 1",
        "icon" : ['far', 'envelope']
    },
    {
        "sublist_id" : "message_2",
        "sublist_label" : "Message 2",
        "icon" : ['far', 'envelope']
    }
];

export const reportingList = [
    {
        "sublist_id" : "report_1",
        "sublist_label" : "Report 1",
        "icon" : ['far', 'chart-bar']
    },
    {
        "sublist_id" : "report_2",
        "sublist_label" : "Report 2",
        "icon" : ['far', 'chart-bar']
    }
];
