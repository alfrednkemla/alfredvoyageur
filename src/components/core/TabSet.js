import React, { useState } from 'react';
import Tab from './Tab';
import styles from '../../css/Tab.module.scss';

export default function TabSet(props) {
    const tabs = props.tabs;
    const [activeTab, setActiveTab] = useState(1);

    function updateActiveTab(tabID) {
        setActiveTab(tabID);
    }

    const tabSet = tabs.map((tab) => {
        return (
            <Tab
                key={tab.id}
                tabID={tab.id}
                name={tab.name}
                isActive={(tab.id === activeTab)}
                updateActiveTab={updateActiveTab}
            />
        )
    });

    let tabContent;
    
    switch (activeTab) {
        case 1:
            tabContent = (<div>Some content</div>);
            break;
        case 2:
            tabContent = (<div>Some different content</div>);
            break;
        case 3:
            tabContent = (<div>Yet more content</div>);
            break;
        default:
            tabContent = (<div/>);
            break;
    }

    return (
        <>
            {tabSet}
            <div className={styles.tabContent}>
                {tabContent}
            </div>
        </>
    );
}