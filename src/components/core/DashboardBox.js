import React from 'react';
import styles from '../../css/DashboardBox.module.scss';

export default function DashboardBox(props) {
    return (
        <div className={styles.dashboardBox}>
            <div className={styles.title}><h2 data-testid="dashboardTitle">{props.title}</h2></div>
            <div className={styles.content} data-testid="dashboardContent">{props.children}</div>
        </div>
    );
}