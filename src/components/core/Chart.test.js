import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';

import Chart from './Chart';

it('Verifies chart displays properly', async () => {
    let data = [];
    for (let i = 0; i < 10; i++) {
        data.push({
            name: 'Var: ' + (i + 1),
            value: (i + 5)
        });
    }

    const { getByTestId } = render(
        <Chart 
            data={data}
            heading="A Chart"
            width={600}
            dataKey="name"
        />
    );

    expect(getByTestId('chartHeading').innerHTML).toEqual('A Chart');
});