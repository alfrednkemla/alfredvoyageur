import React from 'react';
import Icon from "../core/Icon";
import styles from '../../css/SearchInput.module.scss';

export default function SearchInput(props) {
    return (
        <div className={styles.search}>
            <Icon 
                icon={['fas', 'search']}
                style={{
                    color: '#676767',
                    marginRight: '5px',
                    fontSize: '16px'
                }}
            />
            <input id={props.inputID} type="text" placeholder="Search" className={styles.searchInput} />
        </div>
    );
}