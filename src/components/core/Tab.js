import React from 'react';
import styles from '../../css/Tab.module.scss';

export default function Tab(props) {
    return (
        <div className={props.isActive ? styles.drawerTabActive : styles.drawerTab}>
            <div className={styles.tabName} onClick={() => {props.updateActiveTab(props.tabID)}}><span>{props.name}</span></div>
        </div>
    );
}
