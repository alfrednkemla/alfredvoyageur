import React from 'react';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

library.add(fab, fas, far);

export default function Icon(props) {
	return (
		<FontAwesomeIcon style={props.style} icon={props.icon} className={'primary-icon ' + props.class !== undefined ? props.class : ""}/>
	);
}
