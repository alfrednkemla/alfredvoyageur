import React from 'react';

export default function Button(props) {
    const type = props.type ? props.type : 'button';
    const color = props.color ? props.color : 'blue';

    return (
        <button type={type} className={"btn " + color + "-btn"} {...props}>{props.text}</button>
    );
}
