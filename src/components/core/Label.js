import React from 'react';

export default function Label(props) {
    const styles = props.styles ? props.styles : {
        color: '#676767'
    };

    return (
        <label htmlFor={props.for} style={styles}>{props.children}</label>
    );
}