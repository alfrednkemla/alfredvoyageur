import React, { useContext } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './css/styles.scss';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Login from "./pages/Login";
import App from "./pages/App";
import { UserContext } from './context/user';

function Voyageur() {
	const user = useContext(UserContext);

	return (
		<Router basename={process.env.REACT_APP_BASENAME}>
			<div id="voyageur">
				<Switch>
					<Route exact path="/">
						{user.isLoggedIn ? (
							<App/>
						) : (
							<Redirect to={{pathname: "/login"}}/>
						)}
					</Route>
					<Route path="/login">
						<Login/>
					</Route>
				</Switch>
			</div>
		</Router>
	);
}

export default Voyageur;
