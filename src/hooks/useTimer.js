import { useEffect, useState } from 'react';

export default function useTimer() {
	const[seconds, setSeconds] = useState(0);
	useEffect(() => {
		let timeInterval = setInterval(() => {
			setSeconds(seconds => seconds + 1);
		}, 1000);
		return() => clearInterval(timeInterval);
	});
	return seconds;
}
