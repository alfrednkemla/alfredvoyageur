import React from 'react';
import logo from '../assets/login-logo.png';
import LoginForm from '../components/login/LoginForm';
import '../css/login.scss';

export default function Login() {
	return (
		<div id="login" data-testid="login">
			<div id="login-form">
				<div id="login-form-container">
					<img src={logo} alt="logo" />
					<div id="login-form-inputs">
						<LoginForm />
					</div>
				</div>
			</div>
			<div id="login-splash" />
		</div>
	);
}
