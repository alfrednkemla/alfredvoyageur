import React from 'react';
import axios from 'axios';

const axiosObj = axios.create({
    baseURL: process.env.REACT_APP_AXIOS_BASE_URL,
	auth: {
		username: process.env.REACT_APP_AXIOS_AUTH_UNAME,
		password: process.env.REACT_APP_AXIOS_AUTH_PASSWORD
	},
	headers: {
		'api-key': process.env.REACT_APP_AXIOS_API_KEY,
		'Content-Type': 'application/json'
	}
});

export const AxiosContext = React.createContext(axiosObj);
